﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour
{

    //Public reference to the speed and force
    public float maxSpeed;
    public float forceMultipler;

    //Gets the horizontal modifier
    float horizontalInput;

    //Rigidbody reference
    Rigidbody rigidbodyComponent;

    void Awake()
    {

        //Gets a reference to the rigidbody component
        rigidbodyComponent = GetComponent<Rigidbody>();
    }

    void Update()
    {

        //Gets the input for the horizontal movement
        horizontalInput = Input.GetAxisRaw("Horizontal");
    }

    void FixedUpdate()
    {

        //If the paddle reached its top speed...
        if (Mathf.Abs(rigidbodyComponent.velocity.x) < maxSpeed)
        {

            //...sets the input equal to the max speed
            rigidbodyComponent.AddForce(new Vector3(horizontalInput * forceMultipler, 0, 0));
        }
    }






}
