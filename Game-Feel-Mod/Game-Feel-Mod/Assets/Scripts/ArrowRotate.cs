﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowRotate : MonoBehaviour
{

    //Gets a reference to the ball
    public GameObject b;

    //Gets the transform of the child object called direction
    public Transform direction;

    //Gets the public vector3 of the direction of the transform.  Public because it is referenced through other scripts
    public Vector3 dir;

    void Awake()
    {

        //Finds the ball gameobject
        b = GameObject.FindGameObjectWithTag("Ball");
    }

    void Update()
    {

        //As the arrow is alive, it rotates around a base position
        transform.RotateAround(b.GetComponent<Ball>().initialPosition, new Vector3(b.GetComponent<Ball>().initialPosition.x, b.GetComponent<Ball>().initialPosition.y, b.GetComponent<Ball>().initialPosition.z - 10), 120 * Time.deltaTime);
    }

    public Vector3 FindDirection() {

        //Gets the direction from the direction child to the balls initial position
        dir = direction.transform.position - b.GetComponent<Ball>().initialPosition;

        //Starts the kill method
        Kill();

        //Returns the direction value calculated used in the ball script
        return dir;
    }

    void Kill() {

        //Simply destroys the arrow game object
        Destroy(gameObject);
    }
}
