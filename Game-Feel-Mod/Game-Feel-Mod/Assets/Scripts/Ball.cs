﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    //Camera references
    public Camera c;
    Vector3 cameraZoom;
    Vector3 cameraBase;

    //Base scale reference
    float scaleUp = 0.5f;

    //Initial position of ball
    public Vector3 initialPosition;

    //Boolean to prevent repeated changes in momentum
    bool resetHappened = false;

    //Speed and the original speed
    public float speed;
    float baseSpeed;
   
    //Audio of the bounce
    public AudioClip[] bounceSounds;

    //Arrow references
    public GameObject ar;
    public GameObject arrowPrefab;
    Vector3 launchDirection;

    //Paddle references
    public Paddle pa;
    float paddleBaseSpeed;
    float paddleBaseMultiplier;

    //References to components of the ball
    AudioSource asc;
    Rigidbody rbc;

    void Awake() {

        //Set the base spped and multiplier of the paddle for resets
        paddleBaseSpeed = pa.maxSpeed;
        paddleBaseMultiplier = pa.forceMultipler;

        //Gets base values of ball
        baseSpeed = speed;
        initialPosition = transform.position;

        //Gets the base position of the camera
        cameraBase = c.transform.position;
 
        //Gets components of ball
        rbc = GetComponent<Rigidbody>();
        asc = GetComponent<AudioSource>();
    }

    void Start() {

        //Ball goes straight up on the first play through
        rbc.velocity = new Vector3(0, -1, 0);
    }

    void Update()
    {
        //Will update whenever there is a reference to the prefab 
        ar = GameObject.FindGameObjectWithTag("Rotate");

        //If the game was reset and the player presses space, it will launch the ball in the direction of the arrow
        if (resetHappened) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                launchDirection = ar.GetComponent<ArrowRotate>().FindDirection();
                rbc.velocity = launchDirection;
                resetHappened = false;
            }
        }

        //If the player has pressed q, resets the ball's position
        if (Input.GetKeyDown(KeyCode.Q)) {
            Reset();
        }
    }

    void FixedUpdate() {

        //Makes the ball's velocity reasonable
        rbc.velocity = rbc.velocity.normalized * speed;
    }

    void Reset() {

        //Sets the ball's position to the original position
        transform.position = initialPosition;
        
        //ScaleUp is set back down to its base
        scaleUp = 0.5f;

        //Sets the speed of the ball and paddle back to its original
        speed = baseSpeed;
        pa.maxSpeed = paddleBaseSpeed;
        pa.forceMultipler = paddleBaseMultiplier;

        //Stops the ball's velocity
        rbc.velocity =  Vector3.zero;

        //Instantiates an arrow
        Instantiate(arrowPrefab, Vector3.zero, Quaternion.identity);
        
        //Turns the reset boolean to true
        resetHappened = true;
    }

    void OnTriggerEnter(Collider other) {

        //Resets the ball if it hits the dead zone
        if (other.tag == "Dead Zone") {
            Reset();     
        }
    }

    void OnCollisionExit(Collision collision)
    {

        //When the ball hits an object it makes a noise
        asc.PlayOneShot(bounceSounds[Random.Range(0, bounceSounds.Length)]);           
    }

    void OnCollisionEnter(Collision collision)
    {

        //If the ball collides with a paddle, the time slows down in the coroutine
        if (collision.gameObject.tag == "Paddle") {
            StartCoroutine(FreezeTime());
        }
    }

    public IEnumerator FreezeTime() {
        
        //CameraZoom just scales the cameras z position to be closer to the ball
        cameraZoom = new Vector3(this.transform.position.x, this.transform.position.y, c.transform.position.z + (scaleUp * 16));

        //Moves the camera to the CameraZoom position over 2 seconds which is the same amount as the slow down function
        iTween.MoveTo(c.gameObject, iTween.Hash("position", cameraZoom, "time", 2));

        //Sets the time-scale to 100 times slower
        Time.timeScale = 0.01f;

        //Waits for seconds that increases over time with the speeding up of the ball
        yield return new WaitForSecondsRealtime(scaleUp);

        //Scale up increases too
        scaleUp += 0.25f;
        
        //Checks if the timescale is less than 1  then...
        if (Time.timeScale < 1)
        {

            //Resets the timescale
            Time.timeScale = 1f;

            //The speed increases equal to the scaleup value
            speed += scaleUp;

            //Speed up the paddle
            pa.maxSpeed += 5;
            pa.forceMultipler += 50;

            //Resets the camera's position
            iTween.MoveTo(c.gameObject, iTween.Hash("position", cameraBase, "time", 0.2f));
        }
    }
}
