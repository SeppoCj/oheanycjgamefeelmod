﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour
{

    //Gets a reference to the rigidbody and box collider
    Rigidbody rb;
    BoxCollider bc;

    //Instantiates a random roatation value
    Vector3 rotationValue;

    void Awake()
    {

        //Gets the componenets of the Rigidbody and BoxCollider
        rb = GetComponent<Rigidbody>();
        bc = GetComponent<BoxCollider>();

        //Sets the rotationValue to a random z axis value;
        rotationValue = new Vector3(0, 0, Random.Range(360, 2160));
    }

    void OnCollisionEnter(Collision collision)
    {

        //When the ball collides with a brick it sets of a coroutine
        if (collision.gameObject.tag == "Ball")
        {
            StartCoroutine(WaitToDestroy(collision.gameObject));
        }
    }

    IEnumerator WaitToDestroy(GameObject go)
    {

        //Turns the rigidbody to respond to physics
        rb.isKinematic = false;

        //Turns off the box collider
        bc.enabled = false;

        //Sets the force of the brick equal to the ball's velocity
        Vector3 ballForce = go.GetComponent<Rigidbody>().velocity;

        //Multiplies the ballforce of y to the negative value
        ballForce.y *= -1;

        //Adds the force of the balls movement to the brick
        rb.AddForce(ballForce, ForceMode.Impulse);

        //iTween rotate and change color to red
        iTween.RotateTo(this.gameObject, iTween.Hash("rotation", rotationValue, "time", 5f));
        iTween.ColorTo(this.gameObject, iTween.Hash("color", Color.red, "time", 1f));

        //They last for 5 seconds then are destroyed
        yield return new WaitForSecondsRealtime(5f);
        Destroy(gameObject);
    }


}
